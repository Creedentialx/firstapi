# emailer/views.py

import csv
from io import TextIOWrapper

from django.core.mail import send_mail, BadHeaderError
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from .models import EmailRecord

@csrf_exempt
def test_smtp(request):
    subject = "Test mail"
    message = 'Test Form python'
    from_email = 'hq-satai@satoshi-dexai.xyz'
    recipient_list =['creedsallcode@gmail.com']
    send_mail(subject, message, from_email, recipient_list)
    return JsonResponse({'message': 'Emails sent successfully'})
@csrf_exempt
@require_POST

def send_emails(request):
    if 'file' not in request.FILES:
        return JsonResponse({'error': 'No file uploaded'}, status=400)

    file = request.FILES['file']
    reader = csv.reader(TextIOWrapper(file, encoding='utf-8'))
    print(reader)
    
    subject = request.POST.get('subject', 'Default Subject')
    body = request.POST.get('body', 'Default Body')

    for row in reader:
        if len(row) < 1:
            continue
        
        email = row[0]
        try:
            send_mail(subject, '', 'hq-satai@satoshi-dexai.xyz', [email], html_message=body)
            print(email)
            # Save the email record to the database
            EmailRecord.objects.create(recipient=email, subject=subject, body=body)
        except BadHeaderError:
            return JsonResponse({'error': 'Invalid header found.'}, status=400)
        except Exception as e:
            return JsonResponse({'error': str(e)}, status=500)

    return JsonResponse({'message': 'Emails sent successfully'})
